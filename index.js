const express = require('express');
const app = express();
const port = 8000;
const path = require('path');
var mongoose = require('mongoose');
var uri = `mongodb://localhost:27017/CRUD_Pizza365`;

const Drink = require('./app/models/drinkModel');
const Voucher = require('./app/models/voucherModel');

app.use(express.static(`views/Pizza_365`)); // Use this for show image

app.get("/", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/Pizza_365/43.80.html`));
})

mongoose.connect(uri, function (error) {
	if (error) throw error;
	console.log('MongoDB Successfully connected');
})
 
app.listen(port, () => {
  console.log(`6.60 app listening on port ${port}`);
})
